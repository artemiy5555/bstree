import model.Person;

public interface BsTree {

    void clear();

    int size();

    Person[] toArray();

    String toString();

    void add(Person val);

    void del(Person val);

    void getWidth();

    void getHeight();

    int nodes();

    int leaves();

}
