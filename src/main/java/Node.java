import model.Person;

class Node {
    Person value;
    Node left;
    Node right;

    Node(Person value) {
        this.value = value;
        right = null;
        left = null;
    }
}
