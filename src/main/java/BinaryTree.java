import model.Person;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree implements BsTree{

    private Node root;

    private Person[] persons;
    private int i =0;

    private Node addRecursive(Node current, Person value) {
        if (current == null) {
            return new Node(value);
        }

        if (value.getLastName().compareTo(current.value.getLastName()) != -1) {
            current.left = addRecursive(current.left, value);
        } else if (value.getLastName().compareTo(current.value.getLastName()) == -1) {
            current.right = addRecursive(current.right, value);
        } else {
            return current;
        }

        return current;
    }

    public void add(Person value) {
        root = addRecursive(root, value);
    }

    private Node deleteRecursive(Node current, Person value) {
        if (current == null) {
            return null;
        }

        if (value.equals(current.value)) {
            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }
            Person smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }
        if (value.getLastName().compareTo(current.value.getLastName()) == -1) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }

        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private Person findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    public void del(Person value) {
        root = deleteRecursive(root, value);
    }

    public void getHeight(){
        getHeight(root);
    }

    public int nodes() {
        return 0;
    }

    public int leaves() {
        return 0;
    }

    private void getHeight(Node node) {//Порядок обхода состоит в том, чтобы сначала посетить левое поддерево, затем корневой узел и, наконец, правое поддерево
        if (node != null) {
            getHeight(node.left);
            System.out.println(" " + node.value.toString());
            getHeight(node.right);
        }
    }

    public void getWidth() {
        if (root == null) {
            return;
        }

        Queue<Node> nodes = new LinkedList<Node>();
        nodes.add(root);

        while (!nodes.isEmpty()) {

            Node node = nodes.remove();

            System.out.println(" " + node.value.toString());

            if (node.left != null) {
                nodes.add(node.left);
            }

            if (node.right!= null) {
                nodes.add(node.right);
            }
        }
    }

    public void clear() {
        if (root == null) {
            return;
        }
        Queue<Node> nodes = new LinkedList<Node>();
        nodes.add(root);
        while (!nodes.isEmpty()) {
            Node node = nodes.remove();
            System.out.println("Удаление: "+node.value.toString());
            del(node.value);
            if (node.left != null) {
                nodes.add(node.left);
            }
            if (node.right!= null) {
                nodes.add(node.right);
            }
        }
    }

    private void toArray(Node node){
        if (node != null) {
            toArray(node.left);
            persons[i] = node.value;
            i++;
            //System.out.println(" " + node.value.toString());
            toArray(node.right);
        }
    }

    public Person[] toArray() {
        this.i =0;
        persons= new Person[size()];
        toArray(root);
        return persons;
    }

    public int size() {
        return getSizeRecursive(root);
    }

    private int getSizeRecursive(Node current) {
        return current == null ? 0 : getSizeRecursive(current.left) + 1 + getSizeRecursive(current.right);
    }

}
