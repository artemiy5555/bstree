import model.Person;

import java.util.Stack;

public class BinaryTreeF implements BsTree {

    private Node root;

    public void add(Person v) {
        Node x = root, y = null;
        while (x != null) {
            int cmp = v.getLastName().compareTo(x.value.getLastName());
            if (cmp == 0) {
                x.value = v;
                return;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.left;
                } else {
                    x = x.right;
                }
            }
        }
        Node newNode = new Node(v);
        if (y == null) {
            root = newNode;
        } else {
            if (v.getLastName().compareTo(y.value.getLastName()) < 0) {
                y.left = newNode;
            } else {
                y.right = newNode;
            }
        }
    }

    public int size() {
        return getSizeRecursive(root);
    }

    private int getSizeRecursive(Node current) {
        return current == null ? 0 : getSizeRecursive(current.left) + 1 + getSizeRecursive(current.right);
    }

    public void del(Person k) {

        Node x = root;
        Node y = null;
        while (x != null) {
            int cmp = k.getLastName().compareTo(x.value.getLastName());
            if (cmp == 0) {
                break;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.left;
                } else {
                    x = x.right;
                }
            }
        }
        if (x == null) {
            return;
        }
        if (x.right == null) {
            if (y == null) {
                root = x.left;
            } else {
                if (x == y.left) {
                    y.left = x.left;
                } else {
                    y.right = x.left;
                }
            }
        } else {
            Node leftMost = x.right;
            y = null;
            while (leftMost.left != null) {
                y = leftMost;
                leftMost = leftMost.left;
            }
            if (y != null) {
                y.left = leftMost.right;
            } else {
                x.right = leftMost.right;
            }
            x.value.setLastName(leftMost.value.getLastName());
            x.value = leftMost.value;
        }
    }

    public Person get(Person k){

        Node x = root;
        while (x != null) {
            int cmp = k.getLastName().compareTo(x.value.getLastName());
            if (cmp == 0) {
                return x.value;
            }
            if (cmp < 0) {
                x = x.left;
            } else {
                x = x.right;
            }
        }
        return null;
    }

    public void getWidth() {


    }

    public void getHeight() {
        if (root != null) {
            Stack stack = new Stack();
            Node current = root;
            boolean goLeftNext = true;

            stack.push(current);

            while (stack.size() > 0) {
                if (goLeftNext) {
                    while (current.left != null) {
                        stack.push(current);
                        current = current.left;
                    }
                }

                System.out.println(current.value);

                if (current.right != null) {
                    current = current.right;

                    goLeftNext = true;
                } else {
                    current = (Node) stack.pop();
                    goLeftNext = false;
                }
            }

        }
    }

    public int nodes() {
        return 0;
    }

    public int leaves() {
        return 0;
    }

    public Person[] toArray() {
        return new Person[0];
    }

    public void clear() {

    }

}
